package hu.uniobuda.nik.memosnake;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class ColorSequence implements Parcelable{

    ArrayList<Integer> colorSequence;

    public ColorSequence() {
        this.colorSequence = new ArrayList<Integer>();
    }

    public ArrayList<Integer> getColorSequence() {
        return colorSequence;
    }

    public void addNextColor(int nextColor){
        colorSequence.add(nextColor);
    }

    public int getColorFromIndex(int idx){
        return colorSequence.get(idx);
    }

    public int getSize(){
        return colorSequence.size();
    }

    public void clear(){
        this.colorSequence.clear();
    }

    //region parcelabel implementation
    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeList(colorSequence);
    }

    protected ColorSequence(Parcel in) {
        colorSequence = in.readArrayList(null);
    }

    public static final Creator<ColorSequence> CREATOR = new Creator<ColorSequence>() {
        @Override
        public ColorSequence createFromParcel(Parcel in) {
            return new ColorSequence(in);
        }

        @Override
        public ColorSequence[] newArray(int size) {
            return new ColorSequence[size];
        }
    };
    //endregion
}
