package hu.uniobuda.nik.memosnake;

import android.support.annotation.NonNull;

public class HightScoresElement implements Comparable<HightScoresElement>{

    String name;
    int score;

    public HightScoresElement(String name, int score) {
        this.name = name;
        this.score = score;
    }

    public String getName() {
        return name;
    }

    public int getScore() {
        return score;
    }

    @Override
    public int compareTo(@NonNull HightScoresElement hightScoresElement) {
        if(this.score > hightScoresElement.getScore()) return -1;
        else if(this.score > hightScoresElement.getScore()) return 1;
        else return 0;
    }
}
