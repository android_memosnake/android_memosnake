package hu.uniobuda.nik.memosnake;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements UsernameDialog.UsernameDialogListener{

    private Button newGameButton, scoresButton, optionButton;
    private static String felhasznalo;

    public static String getFelhasznalo() {
        return felhasznalo;
    }

    public static void setFelhasznalo(String felhasznalo) {
        MainActivity.felhasznalo = felhasznalo;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        newGameButton = findViewById(R.id.newGameButton);
        scoresButton = findViewById(R.id.scoresButton);
        optionButton = findViewById(R.id.optionButton);
        setFelhasznalo(null);

        newGameButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openNewGameActivity();
            }
        });

        scoresButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openScoreActivity();
            }
        });

        optionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDialog();
            }
        });
    }

    private void openScoreActivity() {
        Intent intent = new Intent(this, HighScoresActivity.class);

        startActivity(intent);

//        PendingIntent pendingIntent =
//                TaskStackBuilder.create(this)
//                        // add all of DetailsActivity's parents to the stack,
//                        // followed by DetailsActivity itself
//                        .addNextIntentWithParentStack(intent)
//                        .getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
//
//        NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
//        builder.setContentIntent(pendingIntent);
    }

    @Override
    public void applyUsername(String username) {

            setFelhasznalo(username);


    }

    public void openNewGameActivity(){

        //openDialog();

        Intent intent = new Intent(this, NewGameActivity.class);
        intent.putExtra("level", 1);
        if(getFelhasznalo() == null || getFelhasznalo() == "")
            setFelhasznalo("User1");
        intent.putExtra("name", getFelhasznalo());

        //aktualis allapot a dev2.0-n

        startActivity(intent);
    }

    public void openDialog() {
        UsernameDialog uD = new UsernameDialog();
        uD.show(getSupportFragmentManager(), "user input");
    }
}
