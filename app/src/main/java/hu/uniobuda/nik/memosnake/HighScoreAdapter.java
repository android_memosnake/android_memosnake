package hu.uniobuda.nik.memosnake;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

public class HighScoreAdapter extends RecyclerView.Adapter<HighScoreAdapter.HighScoreViewHolder> {

    Context mContext;
    ArrayList<HightScoresElement> elements;

    public HighScoreAdapter(Context mContext, ArrayList<HightScoresElement> elements) {
        this.mContext = mContext;
        this.elements = elements;
    }

    @Override
    public HighScoreViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.high_scores_list_layout, null);
        HighScoreViewHolder holder = new HighScoreViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(HighScoreViewHolder holder, int position) {
        HightScoresElement element = elements.get(position);

        holder.nameTextView.setText(element.getName());
        holder.scoresTextView.setText(Integer.toString(element.getScore()));
    }

    @Override
    public int getItemCount() {
        return elements.size();
    }

    class HighScoreViewHolder extends RecyclerView.ViewHolder{

        TextView nameTextView, scoresTextView;

        public HighScoreViewHolder(View itemView) {
            super(itemView);

            nameTextView = itemView.findViewById(R.id.nameTextView);
            scoresTextView = itemView.findViewById(R.id.scoreTextView);
        }
    }
}
