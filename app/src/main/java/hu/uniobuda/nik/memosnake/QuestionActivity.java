package hu.uniobuda.nik.memosnake;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

// import static hu.uniobuda.nik.memosnake.NewGameActivity.sorrendColor;

public class QuestionActivity extends AppCompatActivity {

    ColorSequence colorSequence;

    int level;
    int score;

    public int getLevel() {
        return level;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        colorSequence = intent.getParcelableExtra("color sequence");
        level = intent.getIntExtra("level", 1);
        score = intent.getIntExtra("score", 0);
        setContentView(R.layout.activity_question);
    }

    public ColorSequence getColorSequence() {
        return colorSequence;
    }

    public void nextLevel(int currentScore){
        score += currentScore;
        if(level < 6){
            Intent intent = new Intent(this, NewGameActivity.class);
            intent.putExtra("level", ++level);
            intent.putExtra("score", score);

            startActivity(intent);
            finish();
        }else{
            // TODO Win the game implementation
            writeScore(score, MainActivity.getFelhasznalo());
            //Intent setIntent = new Intent(this , MainActivity.class);
            Toast.makeText(this, "Végig értél a játékon!", Toast.LENGTH_LONG).show();
            //startActivity(setIntent);
            finish();

        }
    }

    public void gameOver() {
        //Intent setIntent = new Intent(this , MainActivity.class);
            //setIntent.addCategory(Intent.CATEGORY_HOME);
            //setIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            //setIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        writeScore(score, MainActivity.getFelhasznalo());
        Toast.makeText(this, "VESZTETTÉL!", Toast.LENGTH_LONG).show();
            //startActivity(setIntent);
            finish();
    }

    public void writeScore(int score, String name) {
        String FILENAME = "scores";
        String saveThis = "@" + name + "@" + score;
        FileOutputStream fos = null;

        try {
            fos = openFileOutput(FILENAME, MODE_APPEND);
            fos.write(saveThis.getBytes());
            fos.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Toast.makeText(this,"back key is pressed", Toast.LENGTH_SHORT).show();
    }
}
