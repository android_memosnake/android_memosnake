package hu.uniobuda.nik.memosnake;

import android.graphics.Color;
import android.graphics.Rect;
import android.support.constraint.solver.widgets.Rectangle;

/**
 * Created by Skom on 2018. 04. 19..
 */

public class SnakePiece {

    Rect geometry;
    int color;

    public SnakePiece(Rect geometry, int left, int top, int width, int height, int color) {
        this.geometry = geometry;
        geometry.left = left;
        geometry.top = top;
        geometry.right = geometry.left + width;
        geometry.bottom = geometry.top + height;
        this.color = color;
    }

    public Rect getGeometry() {
        return geometry;
    }

    public int getColor() {
        return color;
    }
}
