package hu.uniobuda.nik.memosnake;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Random;

public class GameScreen extends View {

    static Random rnd = new Random();
    static final int MAX_QUESTION_COUNT = 2;

    // amit ki kell találni
    ColorSequence colorSequence;
    // a válaszok, hibás válasz esetén törlődik
    ColorSequence answer;
    // a kérdés színei amiből majd keverünk
    LinkedList<Integer> randomColor;
    QuestionActivity qa;

    String question;

    int score;

    // amíg nem épül fel nincs kiterjedése, ennek az első rendernél van szerepe
    boolean loaded;

    int questionOption;

    int probakSzama;

    int height;
    int width;

    int columnSpan;
    int rowSpan;

    ArrayList<SnakePiece> pieces;

    //region View implementation
    public GameScreen(Context context, ColorSequence cs) {
        super(context);

        init(null);
    }

    public GameScreen(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);

        init(attrs);
    }

    public GameScreen(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        init(attrs);
    }

    public GameScreen(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);

        init(attrs);
    }
    //endregion

    private void init(@Nullable AttributeSet set) {
        qa = (QuestionActivity) this.getContext();
        colorSequence = qa.getColorSequence();

        answer = new ColorSequence();
        pieces = new ArrayList<>();

        questionOption = rnd.nextInt(MAX_QUESTION_COUNT ) + 1;

        setQuestionString(questionOption);
        score = 0;
        loaded = false;
        probakSzama = 0;
    }

    private void setQuestionString(int qOpt) {
        switch (qOpt) {
            case 1:
                question = getResources().getString(R.string.FORWARD);
                break;
            case 2:
                question = getResources().getString(R.string.BACKWARD);
        }
    }

    // felépítjük a kígyó darabkáit a szekvencia szerint (ne legyen nagyon több mint 8)
    public void buildSnakeFromSequence() {
        randomColor = arrayListToLinkedList();
        int count = colorSequence.getSize();

        for (int i = 0; i < count; i++) {
            pieces.add(new SnakePiece(
                    new Rect(),
                    ((i % 4) * 2 + 1) * columnSpan,
                    ((i / 4) * 3 + 11) * rowSpan,
                    width / 9,
                    height / 16,
                    getRandomColorFromSequence()
            ));

        }
    }

    // visszaad egy random szít amivel majd az aktuális darabol színezzük
    private int getRandomColorFromSequence() {
        if (randomColor.size() > 0) {
            int idx = rnd.nextInt(randomColor.size());
            int color = randomColor.get(idx);
            randomColor.remove(idx);
            return color;
        } else {
            return Color.TRANSPARENT;
        }
    }

    // test method
//    public void buildSnakeFromSequence(int count){
//        count = 6;
//
//        for (int i = 0; i < count; i++) {
//            pieces.add(new SnakePiece(
//                    new Rect(),
//                    (i+1)*(width / 20) + 100,
//                    66 * (height / 100),
//                    (width / 20) * 10,
//                    (height / 100) * 2,
//                    cf.getRandomColor()
//            ));
//        }
//
//        invalidate();
//    }

    // converter
    public LinkedList<Integer> arrayListToLinkedList() {
        LinkedList<Integer> tmp = new LinkedList<>();

        for (int i = 0; i < colorSequence.getSize(); i++) {
            tmp.add(colorSequence.getColorFromIndex(i));
        }

        return tmp;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        if (!loaded) {
            height = getHeight();
            width = getWidth();
            columnSpan = width / 9;
            rowSpan = height / 16;
            buildSnakeFromSequence();

            loaded = true;
        }

        Paint textPaint = new Paint();
        textPaint.setColor(Color.BLACK);
        textPaint.setStyle(Paint.Style.FILL);
        textPaint.setTextSize(36);
        canvas.drawText(question, 20, 100, textPaint);

        int count = pieces.size();
        Paint paint = new Paint();

        for (int i = 0; i < count; i++) {
            SnakePiece sp = pieces.get(i);

            paint.setColor(sp.color);

            canvas.drawRect(sp.geometry, paint);
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        float touch_x, touch_y;

        int action = event.getAction();

        if (action == MotionEvent.ACTION_DOWN) {
            touch_x = event.getX();
            touch_y = event.getY();
            if (onCollision(touch_x, touch_y)) {
                return true;
            } else {
                return false;
            }
        }

        return false;
    }


    // region Touch Collision
    boolean onCollision(float x, float y) {
        for (int i = 0; i < pieces.size(); i++) {
            if (isCollided(pieces.get(i), x, y)) {
                answer.addNextColor(pieces.get(i).getColor());
                Log.d("COLOR_ADD", "Color added to answer!");
                pieces.remove(pieces.get(i));

                invalidate();

                // check end
                if (answer.getSize() == colorSequence.getSize()) {
                    checkEnd(questionOption);
                }
                return true;
            }
        }
        return false;
    }

    boolean isCollided(SnakePiece sp, float x, float y) {
        Rect sp_geo = sp.getGeometry();
        if (x >= sp_geo.left && x <= sp_geo.right && y >= sp_geo.top && y <= sp_geo.bottom) {
            Log.d("TOUCH", "Snake piece touched!");
            return true;
        } else {
            return false;
        }
    }
    // endregion

    private void checkEnd(int option) {
        boolean lose = false;
        boolean looseVegleg = probakSzama >= 2;

        if (!looseVegleg) {
            switch (option) {
                case 1: //FORWARD
                    for (int i = 0; i < answer.getSize(); i++) {
                        if (answer.getColorFromIndex(i) != colorSequence.getColorFromIndex(i)) {
                            lose = true;
                            break;
                        }
                    }
                    break;
                case 2: //BACKWARD
                    for (int i = 0; i < answer.getSize(); i++) {
                        if (answer.getColorFromIndex(answer.getSize() - i - 1) != colorSequence.getColorFromIndex(i)) {
                            lose = true;
                            break;
                        }
                    }
                    break;
            }

            if (lose) {
                answer.clear();
                buildSnakeFromSequence();
                probakSzama++;

                invalidate();
                Log.d("LOSE", "Rerender!");
            } else {
                // TODO Win implementation
                Log.d("WIN", "You win the game!");
                score = score + ((3 - probakSzama) * 10);
                qa.nextLevel(score);
            }
        } else {
            //openMainMenuAgain();
            qa.gameOver();
            //invalidate();
            Log.d("Over", "vissza..");
        }
    }

    private void openMainMenuAgain() {

        Log.d("OMMA", "openMainMenuAgain Called");
        Intent setIntent = new Intent(GameScreen.this.getContext() , MainActivity.class);
        //setIntent.addCategory(Intent.CATEGORY_HOME);
        setIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        setIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        qa.startActivity(setIntent);
    }
}
