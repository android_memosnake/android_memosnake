package hu.uniobuda.nik.memosnake;

import android.content.Context;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collections;

public class HighScoresActivity extends AppCompatActivity {

    static final String FILENAME = "scores";

    RecyclerView recyclerView;
    HighScoreAdapter adapter;

    ArrayList<HightScoresElement> elements;
    String[] rawDatas;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_high_scores);

        elements = new ArrayList<>();
        rawDatas = readFromFile(FILENAME);
        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

/*        for (int i = 0; i < 10; i++) {
            elements.add(new HightScoresElement(
                    "Test name NO. " + i,
                    i * 2000
            ));
        }*/


        //writeTempScores();
        rawDatas = readFromFile(FILENAME);
        loadRecycleViewInput();

        adapter = new HighScoreAdapter(this, elements);
        recyclerView.setAdapter(adapter);
    }

    // read datas from the file
    private String[] readFromFile(String FILENAME) {

        String[] tmp = null;
        FileInputStream fis = null;

        try {
            fis = openFileInput(FILENAME);
            byte[] buffer = new byte[2048];
            int len;
            while((len = fis.read(buffer)) > 0){
                tmp = (new String(buffer, 1, len - 1)).split("@");
            }
            fis.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return tmp;
    }

    // fill the elements array from the rawDatas and sort it
    private void loadRecycleViewInput(){
        for (int i = 0; i < rawDatas.length - 1; i += 2) {
            try{
                Integer.parseInt(rawDatas[i + 1]);
                elements.add(new HightScoresElement(
                        rawDatas[i],
                        Integer.parseInt(rawDatas[i + 1])
                ));
            }catch (NumberFormatException e){
                elements.add(new HightScoresElement(
                        rawDatas[i],
                        999
                ));
            }
        }

        Collections.sort(elements);
    }

    // write template datas
    private void writeTempScores(){
        String saveThis = "";

        for (int i = 0; i < 10; i++) {
            saveThis += "@AAAAAA@" + (i + 1);
            FileOutputStream fos = null;

            try {
                fos = openFileOutput(FILENAME, MODE_APPEND);
                fos.write(saveThis.getBytes());
                fos.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
