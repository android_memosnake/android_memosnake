package hu.uniobuda.nik.memosnake;

import android.graphics.Color;

import java.lang.reflect.Array;
import java.util.Random;

public class ColorFactory {

    static Random rnd = new Random();

    private int[] colorPalette;

    public ColorFactory() {

        colorPalette = new int[] {
                Color.parseColor("#8bc24a"),
                Color.parseColor("#ffeb3b"),
                Color.parseColor("#ffc107"),
                Color.parseColor("#ff5722"),
                Color.parseColor("#e91e63"),

                Color.parseColor("#259b24"),
                Color.parseColor("#cddc39"),
                Color.parseColor("#ff9800"),
                Color.parseColor("#e51c23"),
                Color.parseColor("#9c27b0"),

                Color.parseColor("#ffeb3b"),
                Color.parseColor("#03a9f4"),
                Color.parseColor("#00bcd4"),
                Color.parseColor("#9e9e9e"),
                Color.parseColor("#607d8b"),

                Color.parseColor("#673ab7"),
                Color.parseColor("#5677fc"),
                Color.parseColor("#009688"),
                Color.parseColor("#795548"),
                Color.parseColor("#212121")
        };
    }

    public int getRandomColor(){
        // return a random color from this array
        return colorPalette[rnd.nextInt(Array.getLength(colorPalette))];
    }
}
