package hu.uniobuda.nik.memosnake;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Random;

public class NewGameActivity extends AppCompatActivity {

    Random rnd = new Random();

    ColorSequence colorSequence = new ColorSequence();
    ArrayList<TextView> textViews;

    RelativeLayout relativeLayout;
    Button rdyBtn;

    TextView timeLimit;
    TextView scoreBox;
    CountDownTimer ct;

    //String felhasznalo;
    //boolean firstTime = true;

    TextView tv2;
    TextView tv3;
    TextView tv4;
    TextView tv5;
    TextView tv6;


    int level;
    int score;
    boolean helyes = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_game);
        Intent intent = getIntent();
        level = intent.getIntExtra("level", 1);
        score = intent.getIntExtra("score", 0);

        scoreBox = findViewById(R.id.scoreBox);
        scoreBox.setText(score + " pont");
        textViews  = new ArrayList<TextView>();
        relativeLayout  = findViewById(R.id.relative_layout);
        rdyBtn = findViewById(R.id.rdy_button);
        timeLimit = findViewById(R.id.timeLimit);

        InitTextboxes(level);

        rdyBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openQuestionActivity();
            }
        });

        ct =  new CountDownTimer(10000, 1000) {

            public void onTick(long millisUntilFinished) {
                timeLimit.setText("Hátralévő idő: " + millisUntilFinished / 1000);
            }

                public void onFinish() {
                openQuestionActivity();
            }
        }.start();

/*
        for (int i = 0; i < textViews.size(); i++) {
            relativeLayout.addView(textViews.get(i));
        }
*/

        /*tv2 = findViewById(R.id.tv2);
        tv3 = findViewById(R.id.tv3);
        tv4 = findViewById(R.id.tv4);
        tv5 = findViewById(R.id.tv5);
        tv6 = findViewById(R.id.tv6);
        ido = findViewById(R.id.ido);
        level = 1;

        int[] colorsOfheSnake = ColorGenerator(level);

        int color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
        tv2.setBackgroundColor(colorsOfheSnake[0]);

        int color3 = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
        //HA ERŐFORRÁSBÓL ADJUK MEG
        //ResourcesCompat.getColor(getResources(), R.color.lila, null);//
        tv3.setBackgroundColor(colorsOfheSnake[1]);

        int color4 = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
        //ResourcesCompat.getColor(getResources(), R.color.citrom, null); //
        tv4.setBackgroundColor(colorsOfheSnake[2]);

        if(level == 1)
        {
            tv5.setBackgroundResource(R.drawable.farka);
            colroSequence.add(color);
            colroSequence.add(color3);
            colroSequence.add(color4);
            new CountDownTimer(10000, 1000) {

                public void onTick(long millisUntilFinished) {
                    ido.setText("Hátralévő idő: " + millisUntilFinished / 1000);
                }

                public void onFinish() {
                    openQuestionActivity();
                }
            }.start();
        }
        else if(level == 2)
        {
            //tv6 a farka
            int color5 = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
            tv5.setBackgroundColor(color5);
            tv6.setBackgroundResource(R.drawable.farka);
        }
*/
    }

    public void openQuestionActivity(){
        Intent intent = new Intent(this, QuestionActivity.class);
        intent.putExtra("color sequence", colorSequence);
        intent.putExtra("level", this.level);
        intent.putExtra("score", score);
        /*if (firstTime) {
            intent.putExtra("name", felhasznalo);
            firstTime = false;
        }*/


        ct.cancel();

        startActivity(intent);
        finish();
    }

    private int[] ColorGenerator(int gameLevel){
        int[] colorValues= new int[gameLevel+2];

        int[] colorPalette={
                Color.parseColor("#8bc24a"),
                Color.parseColor("#ffeb3b"),
                Color.parseColor("#ffc107"),
                Color.parseColor("#ff5722"),
                Color.parseColor("#e91e63"),

                Color.parseColor("#259b24"),
                Color.parseColor("#cddc39"),
                Color.parseColor("#ff9800"),
                Color.parseColor("#e51c23"),
                Color.parseColor("#9c27b0"),

                Color.parseColor("#ffeb3b"),
                Color.parseColor("#03a9f4"),
                Color.parseColor("#00bcd4"),
                Color.parseColor("#9e9e9e"),
                Color.parseColor("#607d8b"),

                Color.parseColor("#673ab7"),
                Color.parseColor("#5677fc"),
                Color.parseColor("#009688"),
                Color.parseColor("#795548"),
                Color.parseColor("#212121")
        };


        for (int i=0; i< Array.getLength(colorValues); i++)
        {
            colorValues[i]= colorPalette[rnd.nextInt(Array.getLength(colorPalette))];
        }
        return colorValues;
    }

    private void InitTextboxes(int lvl){

        ColorFactory cf = new ColorFactory();

        for (int i = 0; i < lvl + 5; i++){
            if(i == 0 || i == 1){
                TextView tv = new TextView(this);
                tv.setBackgroundResource(R.drawable.snakeface);
                RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(
                        ViewGroup.LayoutParams.WRAP_CONTENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT);
                tv.setId(i);

                relativeLayout.addView(tv, lp);

                textViews.add(tv);
            }else if(i == lvl + 4) {
                TextView tv = new TextView(this);
                tv.setId(i);
                tv.setBackgroundResource(R.drawable.tail_corrected);
                RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(
                        ViewGroup.LayoutParams.WRAP_CONTENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT);
                lp.addRule(RelativeLayout.RIGHT_OF, textViews.get(i - 1).getId());
                lp.addRule(RelativeLayout.ALIGN_BOTTOM, textViews.get(i - 1).getId());
                relativeLayout.addView(tv, lp);

                textViews.add(tv);
            }else{
                TextView tv = new TextView(this);
                tv.setId(i);
                int color = cf.getRandomColor();
                tv.setBackgroundColor(color);
                colorSequence.addNextColor(color);
                //tv.setText("asd");

                RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(80, 50);
                lp.addRule(RelativeLayout.RIGHT_OF, textViews.get(i - 1).getId());
                lp.addRule(RelativeLayout.ALIGN_BOTTOM, textViews.get(i - 1).getId());
//                lp.addRule(RelativeLayout.ALIGN_BASELINE, textViews.get(i - 1).getId());
//                lp.addRule(RelativeLayout.END_OF, textViews.get(i - 1).getId());

                relativeLayout.addView(tv, lp);

                textViews.add(tv);
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        ct.cancel();
    }
}
